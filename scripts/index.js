let pets = [
    {
        name: "Rubby",
        type: "Dog",
        breed: "Corgi",
        bestTrick: "Tug of war",
        image: "images/rubby.jpg"
    }, {
        name: "Howdy",
        type: "Dog",
        breed: "Mixed Breed",
        bestTrick: "Go find it!",
        image: "images/howdy.jpg"
    }, {
        name: "KitKit",
        type: "Cat",
        breed: "American Shorthair",
        bestTrick: "Commanding his owner to feed him",
        image: "images/kitkit.jpg"
    }, {
        name: "Lil' Miss",
        type: "Cat",
        breed: "Tabby",
        bestTrick: "Looking aloof",
        image: "images/lilmiss.jpg"
    }, {
        name: "Happy",
        type: "Dog",
        breed: "Golden Retriever",
        bestTrick: "Refusing to retrieve!",
        image: "images/happy.jpg"
    }, {
        name: "Piper",
        type: "Dog",
        breed: "Beagle",
        bestTrick: "Find it!  Dropped food edition!",
        image: "images/piper.jpg"
    }, {
        name: "Spooky",
        type: "Cat",
        breed: "Mixed",
        bestTrick: "Gymnastics!",
        image: "images/spooky.jpg"
    }
];
window.onload = function () {
    loadPetSelect();
}
function loadPetSelect() {
    const petSelect = document.getElementById("petSelect");

    pets.sort(function (a, b) {
        if (a.name < b.name) return -1;
        else if (a.name == b.name) return 0;
        else return 1;
    });
    for (let i = 0; i < pets.length; i++) {
        let theOption = new Option(pets[i].name, undefined);
        petSelect.appendChild(theOption);
    }
    petSelect.onchange = onChangePet;
}
function onChangePet() {
    const petSelect = document.getElementById("petSelect");
    removeCardContents();
    loadPetCard(petSelect.selectedIndex - 1);
}
function loadPetCard(selectedIndex) {
    const mainDiv = document.getElementById("containerDiv");
    const rowDiv = document.getElementById("rowDiv");
    let div = document.createElement("div");
    div.className = "card";
    div.innerHTML = pets[selectedIndex].name;
    rowDiv.appendChild(div);

    // let divTitle = document.createElement("h5");
    // divTitle.className = "card-title";
    // divTitle.innerHTML = pet[selectedIndex].name;

    let newImage = document.createElement("img");
    newImage.src = pets[selectedIndex].image;
    newImage.width = "300";
    div.appendChild(newImage);
}
function removeCardContents() {
    // const rowDiv = document.getElementById("rowDiv");
    // rowDiv.innerHTML = "";
}